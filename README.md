# Nextcloud-Docker

Nextcloud Docker image

## Improvements Over the Official Image

We've implemented several enhancements in our customized Docker image to optimize performance and resource usage:

- **s6-overlay Instead of Supervisord**: We've replaced `supervisord` with `s6-overlay` for better efficiency—reducing Python dependencies, lowering memory footprint, and improving speed.
- **NGINX Instead of Apache**: Our image utilizes `NGINX` instead of `Apache` for improved performance and flexibility.
- **Preconfigured NGINX**: We've fine-tuned the `NGINX` configuration to incorporate the latest security standards and optimizations by utilizing [H5BP NGINX](https://github.com/h5bp/server-configs-nginx).
- **Socket Connection**: `NGINX`, `PHP-FPM`, and `Valkey` are unix socket-connected, promoting efficient communication between components.
- **Streamlined Dockerfile**: We've minimized the number of layers in the Dockerfile, closely aligning with `php:fpm-alpine` and utilizing [docker-php-extension-installer](https://github.com/mlocati/docker-php-extension-installer).
- **PHP Production INI Configuration**: Our image includes the production-ready PHP INI configuration for seamless deployment in production environments.
- **Valkey as PHP Session Handler**: The PHP Session Handler efficiently uses Valkey for sessions.
- **Compatibility with Official Image and Helm Chart**: Our Docker image is virtually compatible with the official image, ensuring usability with the official Helm Chart for Nextcloud.
- **Configured Cron Background Jobs**: We've set up and validated cron background jobs, ensuring scheduled tasks function as intended.
- **Valkey Configuration**: Valkey is properly configured and operational within our image.
- **Dynamic Configuration Copying**: All configurations are dynamically copied from the rootfs folder, eliminating hard-coded entries in the Dockerfile for improved flexibility and maintainability.
- **Less layers**: More optimized image due to low use of Docker layers (I only use one two layers command, whereas nextcloud has 9).

## Client Push(notify_push)) support
The `notify_push` server is automatically initiated and reverse proxified. However, you still need to activate the app and configure it. For detailed instructions, please visit: https://github.com/nextcloud/notify_push#nextcloud-app.