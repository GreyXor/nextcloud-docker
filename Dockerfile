### greyxor/nextcloud-docker ###

### Base image : https://github.com/docker-library/php
FROM php:8.4-fpm-alpine3.21

ENV S6_SYNC_DISKS=1 \
    S6_KEEP_ENV=1

SHELL ["/bin/ash", "-eo", "pipefail", "-c"]

RUN apk add --no-cache nginx=~1.26 \
                       valkey=~7.2 \
                       rsync=~3.4 \
                       bzip2=~1.0 \
                       imagemagick=~7.1 \
                       imagemagick-pdf=~7.1 \
                       imagemagick-jpeg=~7.1 \
                       imagemagick-raw=~7.1 \
                       imagemagick-tiff=~7.1 \
                       imagemagick-heic=~7.1 \
                       imagemagick-webp=~7.1 \
                       imagemagick-svg=~7.1 \
                       ffmpeg=~6.1 \
                       gnupg=~2.4 \
                       s6-overlay=~3.2 && \
### Install NGINX configuration https://github.com/h5bp/server-configs-nginx
    curl -sSL "https://github.com/h5bp/server-configs-nginx/archive/refs/tags/5.0.1.tar.gz" | tar -xzf - --strip-components=1 -C /etc/nginx && \
### Configure PHP https://github.com/mlocati/docker-php-extension-installer
    mv "${PHP_INI_DIR}/php.ini-production" "${PHP_INI_DIR}/php.ini" && \
    curl -sSL https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions -o - | ash -s \
    gd \
    zip \
    opcache \
    pdo_mysql \
    pdo_pgsql \
    bz2 \
    intl \
    ldap \
    smbclient \
    imap \
    bcmath \
    gmp \
    ftp \
    imagick \
    exif \
    sysvsem \
    apcu \
    redis \
    inotify \
    memcached \
    pcntl && \
    mkdir /var/www/data && \
    chown -R www-data:root /var/www && \
    chmod -R g=u /var/www && \
    curl -fsSL -o nextcloud.tar.bz2 "https://download.nextcloud.com/server/releases/nextcloud-31.0.0.tar.bz2" && \
    curl -fsSL -o nextcloud.tar.bz2.asc "https://download.nextcloud.com/server/releases/nextcloud-31.0.0.tar.bz2.asc" && \
    export GNUPGHOME="$(mktemp -d)" && \
# gpg key from https://nextcloud.com/nextcloud.asc
    gpg --batch --keyserver keyserver.ubuntu.com  --recv-keys 28806A878AE423A28372792ED75899B9A724937A && \
    gpg --batch --verify nextcloud.tar.bz2.asc nextcloud.tar.bz2 && \
    tar -xjf nextcloud.tar.bz2 -C /usr/src/ && \
    gpgconf --kill all && \
    rm nextcloud.tar.bz2.asc nextcloud.tar.bz2 && \
    rm -rf "$GNUPGHOME" /usr/src/nextcloud/updater && \
    mkdir -p /usr/src/nextcloud/data && \
    mkdir -p /usr/src/nextcloud/custom_apps && \
    chmod +x /usr/src/nextcloud/occ && \
### Let www-data user listen valkey socket.
    addgroup www-data valkey

COPY /rootfs /

ENTRYPOINT ["/init"]
